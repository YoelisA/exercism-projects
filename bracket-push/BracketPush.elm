module BracketPush exposing (isPaired)

import Dict


isPaired : String -> Bool
isPaired input =
    let
        brackets : Dict.Dict Char Char
        brackets =
            Dict.fromList [ ( '[', ']' ), ( '(', ')' ), ( '{', '}' ) ]
        closing : List Char
        closing =
            Dict.values brackets
        
        paired : List Char -> String -> Bool
        paired openPairs word =
            case String.uncons word of
                Nothing ->
                    List.isEmpty openPairs

                Just ( first, rest ) ->
                    if Dict.member first brackets then
                        paired (first :: openPairs) rest

                    else if List.member first closing then
                        case openPairs of
                            [] ->
                                False

                            open :: restOpen ->
                                Dict.get open brackets == Just first && paired restOpen rest

                    else
                        paired openPairs rest
        
    in
    
    paired [] input


