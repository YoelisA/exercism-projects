module Triangle exposing (..)


type Triangle
    = Equilateral
    | Isosceles
    | Scalene


triangleKind : number -> number -> number -> Result String Triangle
triangleKind x y z =
    
    if areLengthsInvalid x y z then
        Err "Invalid lengths"
    else if isEqualityInvalid x y z then
        Err "Violates inequality"
    else if hasThreeSidesEqual x y z then
        Ok Equilateral
    else if hasTwoSidesEqual x y z then 
        Ok Isosceles
    else if hasNoSideEqual x y z then
        Ok Scalene
    else
        Err "I can't determine the kind of Triangle if you don't give me three numbers greater than 0."

hasThreeSidesEqual : number -> number -> number -> Bool
hasThreeSidesEqual first second third =
    first == second && first == third

hasTwoSidesEqual : number -> number -> number -> Bool
hasTwoSidesEqual first second third =
    first == third || second == third || second == first 

hasNoSideEqual : number -> number -> number -> Bool
hasNoSideEqual first second third =
    first /= second && second /= third

isEqualityInvalid : number -> number -> number -> Bool
isEqualityInvalid first second third = 
    first + second <= third || first + third <= second || second + third <= first

areLengthsInvalid : number -> number -> number -> Bool
areLengthsInvalid first second third =
    first <= 0 || second <= 0 || third <= 0






