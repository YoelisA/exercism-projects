module Accumulate exposing (..)

sum : List Int -> Int
sum list = 
    sumHelp list 0

sumHelp : List Int -> Int -> Int
sumHelp list sumSoFar =
    case list of
        [] ->
            sumSoFar
        first :: rest ->
            first + sumHelp rest sumSoFar

length : List a -> Int
length list =
    lengthHelp list 0

lengthHelp : List a -> Int -> Int
lengthHelp list lengthSoFar = 
    case list of
        [] ->
            lengthSoFar
        first :: rest ->
            1 + lengthHelp rest lengthSoFar

reverse : List a -> List a
reverse list = 
    reverseHelp list []

reverseHelp : List a -> List a -> List a 
reverseHelp list reversedList = 
    case list of
        [] ->
            reversedList
        first :: rest ->
            reverseHelp rest (first :: reversedList )

accumulate : (a -> b) -> List a -> List b
accumulate func input =
    case input of 
        Empty ->
            Empty
        first :: rest ->
            