module Isogram exposing (..)
import Char exposing (..)
import Set exposing (..)
import Tuple exposing (..)
{--Isogram is a solution for the Isogram problem in exercism
--}

--TODO: Try to resolve this problems with sets instead of unique function.
removeAllSymbols : List Char -> List Char
removeAllSymbols rawSentence =
    List.filter Char.isAlpha rawSentence

areListsEqual : List Char -> List Char -> Bool
areListsEqual newList oldList = 
    newList == oldList

-- From krisajenkins/elm-exts/28.0.0
unique : List comparable -> List comparable
unique =
    let
        f x ( seen, result ) =
            if Set.member x seen then
                ( seen, result )

            else
                ( Set.insert x seen, x :: result )
    in
    List.foldl f ( Set.empty, [] )
        >> second
        >> List.reverse

isIsogram : String -> Bool
isIsogram sentence =
    let
        oldSentence = 
            sentence 
                |> String.toLower
                |> String.toList
                |> removeAllSymbols

    in
        oldSentence
            |> unique
            |> areListsEqual oldSentence






