module TwoFer exposing (twoFer)


twoFer : Maybe String -> String
twoFer name =
    case name of 
        Just value ->
            "One for " ++ value ++ ", one for me"
            
        _ ->
            "One for you, one for me"
