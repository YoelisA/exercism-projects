module PhoneNumber exposing (..)
import String exposing (..)
import Char exposing (..)


getNumber : String -> Maybe String
getNumber phoneNumber = 
    if any isAlpha phoneNumber then
         Nothing
    else if String.length phoneNumber >= 10 then
        phoneNumber 
            |> cleanPhoneNumber
            |> checkLength
            |> Maybe.andThen checkAreaCode
            |> Maybe.andThen checkExchangeCode
            |> Maybe.andThen checkSubscriberNumber
    else
        Nothing

cleanPhoneNumber : String -> String
cleanPhoneNumber rawPhoneNumber = 
    rawPhoneNumber 
        |> String.filter Char.isDigit

checkLength : String -> Maybe String
checkLength cleanNumber = 
    if String.length cleanNumber == 11 && startsWith "1" cleanNumber then
        Just (dropLeft 1 cleanNumber)
    else if String.length cleanNumber == 10 then 
        Just cleanNumber
    else
        Nothing

checkAreaCode : String -> Maybe String
checkAreaCode validLengthNumber =
    if startsWith "0" validLengthNumber || startsWith "1" validLengthNumber  then
        Nothing
    else
        Just validLengthNumber

checkExchangeCode : String -> Maybe String
checkExchangeCode validAreaCodeNumber = 
       if List.member 3 ( indexes "1" validAreaCodeNumber ) || List.member 3 ( indexes "0" validAreaCodeNumber ) then
            Nothing
        else
            Just validAreaCodeNumber

checkSubscriberNumber : String -> Maybe String
checkSubscriberNumber validExchangeCodeNumber = 
    Just validExchangeCodeNumber



