module Accumulate exposing (accumulate)


accumulate : (a -> b) -> List a -> List b
accumulate func input =
    List.foldr (\x acc -> (::) (func x) acc) [] input