module Hamming exposing (..)
import List exposing (..)
import String exposing (..)
import Tuple exposing (..)

distance : String -> String -> Result String Int
distance left right =
    if String.length left /= String.length right then
        Err "left and right strands must be of equal length"
    else
        List.map2 Tuple.pair (String.toList left) (String.toList right)
            |> List.filter (\(a, b) -> a /= b)
            |> List.length
            |> Ok
