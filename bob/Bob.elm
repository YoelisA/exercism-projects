module Bob exposing (hey)


hey : String -> String
hey remark =
    let
        isEmpty input = 
            
            String.isEmpty (String.trim (input)) || (String.contains "\\" input )
        
        isYelledQuestion input =
            (String.toUpper input == input) && String.endsWith "?" input && String.any Char.isAlpha input
        isQuestion input =
            String.endsWith "?" (String.trim input) && String.length input > 1
        isYelled input = 
            String.toUpper input == input && (String.any Char.isAlpha input)
        

    in
        if isEmpty remark then
            "Fine. Be that way!"
        else if isYelledQuestion remark then 
            "Calm down, I know what I'm doing!"
        else if isQuestion remark then 
            "Sure."
        else if isYelled remark then
            "Whoa, chill out!" 
        else
            "Whatever."

