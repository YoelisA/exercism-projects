module ScrabbleScore exposing (..)
import Dict exposing (fromList)
import List exposing (..)
import String exposing (..)
import Tuple exposing (..)
-- TODO: Try to implement this algorithm with uncons
-- TODO: Try to implement this algorithm with foldl
-- TODO: Try to remove the Maybe.withDefault line 51
table : Dict.Dict Char Int
table = 
    Dict.fromList 
        [ ( 'A', 1 )
        , ( 'E', 1 )
        , ( 'I', 1 )
        , ( 'O', 1 )
        , ( 'U', 1 )
        , ( 'L', 1 )
        , ( 'N', 1 )
        , ( 'R', 1 )
        , ( 'S', 1 )
        , ( 'T', 1 )
        , ( 'D', 2 )
        , ( 'G', 2 )
        , ( 'B', 3 )
        , ( 'C', 3 )
        , ( 'M', 3 )
        , ( 'P', 3 )
        , ( 'F', 4 )
        , ( 'H', 4 )
        , ( 'V', 4 )
        , ( 'W', 4 )
        , ( 'Y', 4 )
        , ( 'K', 5 )
        , ( 'J', 8 )
        , ( 'X', 8 )
        , ( 'Q', 10 )
        , ( 'Z', 10 )
        ]

scoreWord : String -> Int
scoreWord word =
    let 
        wordToAnalyse = word
            |> String.toUpper
            |> String.toList
    in
        case wordToAnalyse of
            [] ->
                0
            first :: rest ->
                Maybe.withDefault 0 (Dict.get first table) + scoreWord (String.fromList rest)

    
