module Leap exposing (..)

type Year
    = Leap
    | NotLeap


isLeapYear : Int -> Bool
isLeapYear year =
    
    let
        requestTypeOfYear x =
        
            case ( isDivisibleBy4 year, isDivisibleBy100 year, isDivisibleBy400 year ) of
                
                ( True, False, False ) ->
                    Leap
                
                ( True, True, False ) ->
                    NotLeap
                
                ( True , True , True ) ->
                    Leap
                
                _ ->
                    NotLeap
    in
        case requestTypeOfYear year of 
            Leap ->
                True
            NotLeap ->
                False

isDivisibleBy4 =
    modBy 4 >> (==) 0

isDivisibleBy100 =
    modBy 100 >> (==) 0

isDivisibleBy400 = 
    modBy 400 >> (==) 0

