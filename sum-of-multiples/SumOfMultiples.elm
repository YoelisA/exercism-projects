module SumOfMultiples exposing (sumOfMultiples)

import List exposing (..)

sumOfMultiples : List Int -> Int -> Int
sumOfMultiples multiple limit =
    let
        myNumbers =
            range 0 (limit - 1)

        isDivisibleBy =
            \number ->
                List.any ( \multiples -> modBy multiples number == 0 ) multiple
    in
    case multiple of
        x :: xs ->
            myNumbers
                |> List.filter isDivisibleBy
                |> List.sum
        _ ->
            0
